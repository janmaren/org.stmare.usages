package org.stmare.util;

public class StringUtil {
    private static final String CLASSPATH2 = "classpath*:";

    private static final String CLASSPATH = "classpath:";

    private static String terminatorBegin = "\r\n\t>\"';>< ,=()";

    private static String terminatorEnd = terminatorBegin;//"\r\n\t>\"';< ,=";

    
    /**
     * Given string and position pointing into begining of last part
     * Get whole link by finding start and checking end if there is terminator behind or end of file
     * @param content text file
     * @param startStringPos position of last part of link string
     * @param endStringPos position of character behind link (should be terminator or end of file)
     * @return link or null when there is no terminator at the end
     */
    public static String getLinkStringByLastPart(String content, int startStringPos, int endStringPos) {
        int start = startStringPos - 1;
        while (start >= 0) {
            char charAt = content.charAt(start);
            if (charAt == ':') {
                int startPosClasspath = getStartPos(CLASSPATH, content, start);
                if (startPosClasspath == -1) {
                    startPosClasspath = getStartPos(CLASSPATH2, content, start);
                }
                if (startPosClasspath == -1) {
                    break;
                }
                char charAt2 = content.charAt(startPosClasspath);
                if (terminatorBegin.indexOf(charAt2) != -1) {
                    // using also 'classpath[*]:'
                    start = startPosClasspath;
                } // else ':' is terminator
                break;
            } else if (terminatorBegin.indexOf(charAt) != -1) {
                break;
            }
            start --;
        }
        
        int end = endStringPos;
        if (end < content.length()) {
            // there is other char following which must be terminator
            char charAt = content.charAt(end);
            if (terminatorEnd.indexOf(charAt) == -1) {
                return null; // but it's not terminator, the text continues
            }
        }
        
        // start is previous terminator so + 1
        return content.substring(start + 1, end);
    }
    
    /**
     * Returns true when substring of content has terminator characters around
     * (If false then it means the substring has prefix or suffix)
     * @param content
     * @param startStringPos
     * @param endStringPos
     * @return
     */
    public static boolean isTerminatorAround(String content, int startStringPos, int endStringPos) {
        if (startStringPos > 0) {
            char charAt = content.charAt(startStringPos - 1);
            if (terminatorBegin.indexOf(charAt) == -1) {
                return false;
            }
        }
        
        if (endStringPos < content.length()) {
            char charAt = content.charAt(endStringPos);
            if (terminatorEnd.indexOf(charAt) == -1) {
                return false;
            }
        }
        
        return true;
    }
    
    private static int getStartPos(String validString, String string, int pos) {
        int validStringPos = validString.length() - 1;
        int stringPos = pos;
        while (validStringPos >= 0 && stringPos >= 0) {
            char validStringChar = validString.charAt(validStringPos);
            char stringChar = string.charAt(stringPos);
            if (validStringChar != stringChar) {
                return -1;
            }
            validStringPos --;
            stringPos --;
        }
        
        if (validStringPos == -1) {
            return pos - validString.length();
        }
        return -2;
    }
    
    private static boolean isPrefix(String validString, String string, int pos) {
        int validStringPos = validString.length() - 1;
        int stringPos = pos;
        while (validStringPos >= 0 && stringPos >= 0) {
            char validStringChar = validString.charAt(validStringPos);
            char stringChar = string.charAt(stringPos);
            if (validStringChar != stringChar) {
                return false;
            }
            validStringPos --;
            stringPos --;
        }
        
        if (validStringPos == -1) {
            return true;
        }
        return false;
    }
    
    /**
     * Returns text which is around offset in string
     * It returns null value when empty text would be returned
     * @param content
     * @param offset
     * @return
     */
    public static LinkTextInfo getLinkTextInfo(String content, int offset) {
        if (content.length() > offset && terminatorEnd.indexOf(content.charAt(offset)) != -1) {
            // cursor at terminator
            return null;
        }
        int start = offset - 1;
        while (start >= 0) {
            char charAt = content.charAt(start);
            if (charAt == ':') {
                if (isPrefix(CLASSPATH, content, start)) {
                    int here = start - CLASSPATH.length();
                    if (here < 0 || terminatorBegin.indexOf(content.charAt(here)) >= 0) {
                        start = here;
                        break;
                    }
                } else 
                if (isPrefix(CLASSPATH2, content, start)) {
                    int here = start - CLASSPATH2.length();
                    if (here < 0 || terminatorBegin.indexOf(content.charAt(here)) >= 0) {
                        start = here;
                        break;
                    }
                }
                break;
            } else if (terminatorBegin.indexOf(charAt) != -1) {
                break;
            }
            start --;
        }
        start ++;
        String str = content.substring(start);
        int end;
        if (str.startsWith(CLASSPATH)) {
            end = start + CLASSPATH.length();
        } else if (str.startsWith(CLASSPATH2)){
            end = start + CLASSPATH.length();
        } else {
            end = offset;
        }
        
        while (end < content.length()) {
            char charAt = content.charAt(end);
            if (terminatorEnd.indexOf(charAt) != -1) {
                break;
            }
            end ++;
        }

        String substr = content.substring(start, end);
        if (substr.length() == 0) {
            return null;
        }
        if (str.startsWith(CLASSPATH) && str.length() == CLASSPATH.length()) {
            return null;
        }
        if (str.startsWith(CLASSPATH2) && str.length() == CLASSPATH2.length()){
            return null;
        } 
        
        LinkTextInfo textInfo = new LinkTextInfo();
        textInfo.setStart(start);
        textInfo.setLength(end - start);
        textInfo.setValue(substr);
        
        return textInfo;
    }    
}
