package org.stmare.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;

public class VariousHelper {
    public static <T> List<T> mergeList(List<T> ... merged) {
        List<T> list = new ArrayList<T>(merged[0]);
        
        for (int i = 1; i < merged.length; i++) {
            List<T> mList = merged[1];
            for (T t0 : mList) {
                boolean found = false;
                for (T t1 : list) {
                    if (t1.equals(t0)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    list.add(t0);
                }
            }
        }
        
        return list;
    }
    
    public static <T> void addWhenNotInList(List<T> list, T toAdd) {
        if (!list.contains(toAdd)) {
            list.add(toAdd);
        }
    }
    
    public static boolean isPositionInComment(String str, int pos) {
        return occurenceNumberBefore("<!--", str, pos) - occurenceNumberBefore("-->", str, pos) > 0;
    }

    public static int occurenceNumberBefore(String search, String str, int fromPos)
    {
        int number = 0;
        int pos = -1;

        while (pos < fromPos) {
            pos = str.indexOf(search, pos + 1);
            if (pos == -1)
                break;
            if (pos < fromPos)
                number++;
        }

        return number;
    }


    public static int findClosingCharPos(String text, int fromPos) {
    	char ch = text.charAt(fromPos);
    	while (fromPos < text.length()) {
    		ch = text.charAt(fromPos);
    		if (ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n') {
    			fromPos++;
    			continue;
    		}
    		if (ch == '\'' || ch == '"' | ch == '<')
    		    return fromPos;
    		fromPos++;
    	}
    	return -1;
    }
    
    /**
     * Find tag name of attribute
     * @param textFileContent
     * @param pos
     * @return
     */
    public static String findTagName(String textFileContent, int pos) {
    	// find '=' before attribute value
    	pos--;
    	while (pos > 0) {
    		char charAt = textFileContent.charAt(pos);
    		if (charAt == '=') {
    			break;
    		}
    		else if (charAt == ' ' || charAt == '\t' || charAt == '\r' || charAt == '\n') {
    			pos--;
    		}
    		else return null;
    	}
    	if (pos < 0)
    		return null;
    
    	while (pos > 0) {
    		char charAt = textFileContent.charAt(pos);
    		if (charAt == '<')
    			break;
    		else if (charAt == '>')
    			return null;
    		else pos--;
    	}
    	if (pos < 0)
    		return null;
    
    	pos ++;
    	int beg = pos;
    	while (pos < textFileContent.length()) {
    		char charAt = textFileContent.charAt(pos);
    		if ((charAt >= 'A' && charAt <= 'Z') || (charAt >= 'a' && charAt <= 'z')
    				|| (charAt >= '0' && charAt <= '9') || charAt == ':') {
    			pos++;
    		} else break;
    	}
    	if (pos - beg == 0 || pos == textFileContent.length())
    		return null;
    	return textFileContent.substring(beg, pos);
    }

    /**
     * Reads file to string including properly set charset
     * return null when unable to read
     * @param file
     * @return
     */
    public static String readFileAsString(IFile file) {
        StringBuilder fileData = new StringBuilder(4096);
        BufferedReader reader = null;
        try {
            InputStream is = file.getContents();
            InputStreamReader isr = new InputStreamReader(is, file.getCharset());
            reader = new BufferedReader(isr);
            char[] buffer = new char[4096];
            int numRead = 0;
            while ((numRead = reader.read(buffer)) != -1) {
                fileData.append(buffer, 0, numRead);
            }
            return fileData.toString();
        } catch (CoreException e) {
        } catch (UnsupportedEncodingException e) {
        } catch (IOException e) {
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
        }
        return null;
    }
}
