package org.stmare.hyperlink;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.hyperlink.AbstractHyperlinkDetector;
import org.eclipse.jface.text.hyperlink.IHyperlink;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.stmare.usages.Activator;
import org.stmare.usages.view.Messages;
import org.stmare.util.LinkTextInfo;
import org.stmare.util.StringUtil;

public class ResourceHyperlinkDetector extends AbstractHyperlinkDetector {
    /**
     * Time after the cache expires
     */
    private static final long CACHE_TIMEOUT = 10000; // 10 sec
    
    /**
     * Caching results
     */
    private Map<String, CachedInfo> cachedReferencedFiles = new HashMap<String, CachedInfo>();
    
    @Override
    public IHyperlink[] detectHyperlinks(ITextViewer viewer, final IRegion region, boolean canShowMultipleHyperlinks) {
        Activator.logDebug("Finding links");
        IFile referencingFile = getReferencingFile();
        if (referencingFile == null) {
            return null;
        }
        IDocument document = viewer.getDocument();
        String stringFile = document.get();
        Activator.logDebug("getLinkTextInfo started");
        org.stmare.util.LinkTextInfo linkTextInfo = StringUtil.getLinkTextInfo(stringFile, region.getOffset());
        Activator.logDebug("getLinkTextInfo found " + (linkTextInfo == null ? "null" : linkTextInfo.getValue()));
        
        if (linkTextInfo != null) { // somewhere inside of node
            List<IFile> referencedFiles = findFiles(referencingFile, linkTextInfo.getValue());
            if (referencedFiles != null && referencedFiles.size() > 0) {
                // able to generate hyperlink with at least 1 value so let's do it
                IHyperlink[] hyperlinks = null;
                hyperlinks = buildHyperlinks(referencedFiles, linkTextInfo);
                if (!canShowMultipleHyperlinks) {
                    IHyperlink[] hyperlinksOne = new IHyperlink[1];
                    hyperlinksOne[0] = hyperlinks[0];
                    Activator.logDebug("Found hyperlink");
                    return hyperlinksOne;
                }
                Activator.logDebug("Found hyperlinks");
                return hyperlinks;
            }
        }
        Activator.logDebug("Nothing found");
        return null;
    }

    /**
     * Build hyperlink array for found links
     * @param referencedFiles
     * @param linkTextInfo
     * @return
     */
    public IHyperlink[] buildHyperlinks(List<IFile> referencedFiles, final LinkTextInfo linkTextInfo) {
        int i = 0;
        IHyperlink[] hyperlinks = new IHyperlink[referencedFiles.size()];
        for (final IFile referencedFile : referencedFiles) {
            IHyperlink hyperLink = new IHyperlink() {
                @Override
                public IRegion getHyperlinkRegion() {
                    IRegion region = new IRegion() {
                        @Override
                        public int getLength() {
                            return linkTextInfo.getLength();
                        }
   
                        @Override
                        public int getOffset() {
                            return linkTextInfo.getStart();
                        }};
                    return region;
                }
   
                @Override
                public String getHyperlinkText() {
                    return Messages.ResourceHyperlinkDetector_open + referencedFile.getFullPath().toString();
                }
   
                @Override
                public String getTypeLabel() {
                    return Messages.ResourceHyperlinkDetector_resource;
                }
   
                @Override
                public void open() {
                    IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
                    try {
                        IDE.openEditor(page, referencedFile);
                    } catch (PartInitException e1) {
                        Activator.logError(e1);
                    }
                }
                
            };
            hyperlinks[i++] = hyperLink;
        }
        return hyperlinks;
    }

    /**
     * Return file with hyperlink
     * @return
     */
    public static IFile getReferencingFile() {
        IFile file = null;
        IWorkbench workbench = PlatformUI.getWorkbench();
        IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
        if (window == null) {
            return null;
        }
        IWorkbenchPage page = window.getActivePage();
        if (page == null) {
            return null;
        }
        IEditorPart activeEditor = page.getActiveEditor();
        if (activeEditor == null) {
            return null;
        }
        IEditorInput editorInput = activeEditor.getEditorInput();
        if (editorInput instanceof IFileEditorInput) {
            IFileEditorInput fei = (IFileEditorInput) editorInput;
            file = fei.getFile();
        }
        return file;
    }

    /**
     * Use cache to return results or manage new find
     * @param referencingFile
     * @param value
     * @return
     */
    private List<IFile> findFiles(IFile referencingFile, String value) {
        Activator.logDebug("Finding resources for: " + referencingFile + ", string: " + value);
        cleanCache();
        CachedInfo cachedInfo = cachedReferencedFiles.get(referencingFile + ":" + value); //$NON-NLS-1$
        if (cachedInfo != null) {
            Activator.logDebug("Using cache, returnig count: " + cachedInfo.referencedFiles.size());
            return cachedInfo.referencedFiles;
        }
        Activator.logDebug("Not found in cache - do search");
        ResourceFinder resourceFinder = new ResourceFinder(referencingFile, value);
        List<IFile> referencedFiles = resourceFinder.find();
        cachedInfo = new CachedInfo();
        cachedInfo.validUntilTimestamp = System.currentTimeMillis() + CACHE_TIMEOUT;
        cachedInfo.referencedFiles = referencedFiles;
        cachedReferencedFiles.put(referencingFile + ":" + value, cachedInfo); //$NON-NLS-1$
        return referencedFiles;
    }

    /**
     * Expired results remove
     */
    private void cleanCache() {
        Map<String, CachedInfo> tmpCachedInfos = new HashMap<String, ResourceHyperlinkDetector.CachedInfo>();
        Set<String> keySet = cachedReferencedFiles.keySet();
        for (String key : keySet) {
            CachedInfo cachedInfo = cachedReferencedFiles.get(key);
            if (cachedInfo.validUntilTimestamp >= System.currentTimeMillis()) {
                tmpCachedInfos.put(key, cachedInfo);
            }
        }
        cachedReferencedFiles = tmpCachedInfos;
    }
    
    /**
     * Caching info 
     */
    static class CachedInfo {
        /**
         * Result files
         */
        public List<IFile> referencedFiles;
        
        /**
         * Until this timestamp the result files are valid.
         */
        public long validUntilTimestamp;
    }
}
