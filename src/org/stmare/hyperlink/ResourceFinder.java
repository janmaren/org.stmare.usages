package org.stmare.hyperlink;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.search.IJavaSearchConstants;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.jdt.core.search.SearchMatch;
import org.eclipse.jdt.core.search.SearchParticipant;
import org.eclipse.jdt.core.search.SearchPattern;
import org.eclipse.jdt.core.search.SearchRequestor;
import org.eclipse.search.core.text.TextSearchEngine;
import org.eclipse.search.core.text.TextSearchRequestor;
import org.eclipse.search.ui.text.FileTextSearchScope;
import org.stmare.usages.Activator;
import org.stmare.wst.J2EEUtilsAdapter;

/**
 * Finds references to all possible files for which the resource string matches 
 */
public class ResourceFinder {
    private static final String CLASSPATH2 = "classpath*:";
    private static final String CLASSPATH = "classpath:";
    private String resource;
    private IFile referencingFile;
    private List<IFile> referencedFiles = new ArrayList<IFile>();
    private static final Pattern FULL_CLASS_PATTERN = Pattern.compile("^([a-z][a-z_0-9]*\\.)*[A-Z_]($[A-Z_]|[\\w_])*$");
    
    public ResourceFinder(IFile referencingFile, String resource) {
        super();
        this.referencingFile = referencingFile;
        this.resource = resource;
    }
    
    public List<IFile> find() {
        if (resource.startsWith(CLASSPATH) || resource.startsWith(CLASSPATH2)) {
            Activator.logDebug("Searching classpath resource " + resource);
            String res = null;
            if (resource.startsWith(CLASSPATH)) {
                res = resource.substring(CLASSPATH.length());
            } else if (resource.startsWith(CLASSPATH2)) {
                res = resource.substring(CLASSPATH2.length());
            } else {
                throw new RuntimeException("Unknown classpath prefix");
            }
            // classpath resources searching in all classpath directories of all projects
            referencedFiles = searchResourceOnSourceDirsOfProjects(referencingFile.getWorkspace(), res);
            Activator.logDebug("Searching classpath resource " + resource + " finished");
        } else {
            if (resource.startsWith("/")) {
                Activator.logDebug("Searching noneclasspath resource " + resource + " starting by /");
                // supposing it's in current project
                referencedFiles = getIFilesInProject(referencingFile.getProject(), resource);
                if (referencedFiles.size() == 0) {
                    // try search source folders of all projects
                    referencedFiles = searchResourceOnSourceDirsOfProjects(referencingFile.getWorkspace(), resource);
                }
                Activator.logDebug("Searching noneclasspath resource " + resource + " starting by / finished");
            } else {
                Activator.logDebug("Searching noneclasspath resource " + resource + " not starting by /");
                IFile file = referencingFile.getParent().getFile(new Path(resource));
                if (file != null && file.exists()) {
                    referencedFiles.add(file);
                } else {
                    Activator.logDebug("Searching class " + resource);
                    // try search class
                    referencedFiles = searchJavaOnSourceDirsOfProjects(referencingFile.getWorkspace(), resource);
                    Activator.logDebug("Searching class " + resource + " finished, count is " + referencedFiles.size());
                    if (referencedFiles.size() == 0) {
                        Activator.logDebug("Searching resource " + resource);
                        // try all source folders of current project
                        referencedFiles = getIFilesInProject(referencingFile.getProject(), resource);
                        Activator.logDebug("Searching resource " + resource + " finished, resutrned count " + referencedFiles.size());
                        if (referencedFiles.size() == 0) {
                            Activator.logDebug("Searching source folders of all projects for " + resource);
                            // try search source folders of all projects
                            referencedFiles = searchResourceOnSourceDirsOfProjects(referencingFile.getWorkspace(), resource);
                            Activator.logDebug("Searching source folders of all projects for " + resource + " finished");
                        }
                    }
                }
                Activator.logDebug("Searching noneclasspath resource " + resource + " not starting by / finished");
            }
        }
        return referencedFiles;
    }

    /**
     * Get matching files which can be on all source directories in all projects
     * @param workspace
     * @param referencedResourcePath path in a source directory 
     * @return
     */
    private List<IFile> searchResourceOnSourceDirsOfProjects(IWorkspace workspace, String referencedResourcePath) {
        final List<IFile> foundIFiles = new ArrayList<IFile>();
        int lastIndexOf = referencedResourcePath.lastIndexOf("/");
        String resourceName = referencedResourcePath;
        if (lastIndexOf != -1) {
            // searching only file without path
            if (lastIndexOf == resourceName.length() - 1) {
                return foundIFiles;
            }
            resourceName = resourceName.substring(lastIndexOf + 1); 
        }
        String[] propPatterns = {resourceName};
        FileTextSearchScope scope = FileTextSearchScope.newWorkspaceScope(propPatterns, false);
        TextSearchRequestor requestor = new TextSearchRequestor() {
            @Override
            public boolean acceptFile(IFile file) throws CoreException {
                foundIFiles.add(file);
                return true;
            }
        };
        
        Pattern pattern = Pattern.compile("");
        TextSearchEngine.create().search(scope, requestor, pattern, null);
        
        List<IFile> resultIFiles = filterMatchingFiles(foundIFiles, workspace, referencedResourcePath);

        return resultIFiles;
    }
    
    private List<IFile> searchJavaOnSourceDirsOfProjects(IWorkspace workspace, String referencedClass) {
        final List<IFile> resultIFiles = new ArrayList<IFile>();
        if (!isClassName(referencedClass)) {
            return resultIFiles;
        }
        SearchPattern pattern = SearchPattern.createPattern(referencedClass, IJavaSearchConstants.TYPE, IJavaSearchConstants.DECLARATIONS, SearchPattern.R_PATTERN_MATCH | SearchPattern.R_CASE_SENSITIVE);
        if (pattern == null) {
            Activator.logWarn("Pattern for referencedClass '" + referencedClass + "' is null - nothing will be found");
            return resultIFiles;
        }
        IJavaSearchScope scope = SearchEngine.createWorkspaceScope();
        SearchEngine searchEngine = new SearchEngine();
        SearchRequestor requestor = new SearchRequestor() {
            @Override
            public void acceptSearchMatch(SearchMatch match) throws CoreException {
                if (match.getResource() instanceof IFile) {
                    resultIFiles.add((IFile) match.getResource());
                }
            }};
        try {
            searchEngine.search(pattern, new SearchParticipant[] {SearchEngine.getDefaultSearchParticipant()}, scope, requestor, null);
        } catch (CoreException e) {
            // no handling
        }
        
        return resultIFiles;
    }
    
    /**
     * Searched files needn't correspond with resource path in source directory (search by name not by path) so select only corresponding files
     * Selected files are selected from source paths
     * @param searchedFiles
     * @param workspace
     * @param resource
     * @return
     */
    private static List<IFile> filterMatchingFiles(List<IFile> searchedFiles, IWorkspace workspace, String resource) {
        ArrayList<IFile> resultIFiles = new ArrayList<IFile>();
        IWorkspaceRoot workspaceRoot = workspace.getRoot();
        IProject[] projects = workspaceRoot.getProjects();
        for (IProject iProject : projects) {
            IJavaProject javaProject = JavaCore.create(iProject);
            try {
                IClasspathEntry[] rawClasspath = javaProject.getRawClasspath();
                for (IClasspathEntry iClasspathEntry : rawClasspath) {
                    if (iClasspathEntry.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
                        IPath path = iClasspathEntry.getPath();
                        IFolder folder = workspaceRoot.getFolder(path);
                        IFile file = folder.getFile(resource);
                        if (searchedFiles.contains(file)) {
                            resultIFiles.add(file);
                        }
                    }
                }
            } catch (JavaModelException e1) {
                // we ignore weird project, no handling 
            }
            
        }
        return resultIFiles;
    }
    /**
     * Returns all existing files from classpath or web content of given project
     * @param iProject
     * @param resource
     * @return
     */
    private static List<IFile> getIFilesInProject(IProject iProject, String resource) {
        if (resource.startsWith("/")) {
            resource = resource.substring(1);
        }
        IWorkspaceRoot workspaceRoot = iProject.getWorkspace().getRoot();
        ArrayList<IFile> resultIFiles = new ArrayList<IFile>();
        IJavaProject javaProject = JavaCore.create(iProject);
        try {
            // classpath
            IClasspathEntry[] rawClasspath = javaProject.getRawClasspath();
            for (IClasspathEntry iClasspathEntry : rawClasspath) {
                if (iClasspathEntry.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
                    IPath path = iClasspathEntry.getPath();
                    IFolder folder = workspaceRoot.getFolder(path);
                    IFile file = folder.getFile(resource);
                    if (file != null && file.exists()) {
                        resultIFiles.add(file);
                    }
                }
            }
            
            // web content
            if (J2EEUtilsAdapter.isWebComponent(iProject)) {
                IContainer webContentContainer = J2EEUtilsAdapter.getWebContentContainer(iProject);
                if (webContentContainer != null) {
                    IFile file = webContentContainer.getFile(new Path(resource));
                    if (file != null && file.exists()) {
                        resultIFiles.add(file);
                    }
                }
            }
        } catch (JavaModelException e1) {
            // when current project is not a project but only a container of other modules then
            // such exception occures. This plugin doesn't support to find references
            // for container
        }
        return resultIFiles;
    }
    
    private static boolean isClassName(String str) {
        Matcher matcher = FULL_CLASS_PATTERN.matcher(str);
        return matcher.matches();
    }
}
