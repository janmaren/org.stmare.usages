package org.stmare.wst;

import java.lang.reflect.Method;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IProject;

/**
 * Access to WST J2EEUtils by reflection (dependency needn't exist so avoid class loading errors)  
 */
public class J2EEUtilsAdapter {
    private static Class<?> J2EEUtilsClazz = null;
    private static Method isWebComponentMethod = null;
    private static Method getWebContentContainerMethod = null;
    
    static {
        try {
            J2EEUtilsClazz = Class.forName("org.eclipse.jst.ws.internal.common.J2EEUtils");
            isWebComponentMethod = J2EEUtilsClazz.getMethod("isWebComponent", IProject.class);
            getWebContentContainerMethod = J2EEUtilsClazz.getMethod("getWebContentContainer", IProject.class);
        } catch (Exception e) {
            // no handling
        }
    }
    
    public static boolean isWebComponent(IProject iProject) {
        if (isWebComponentMethod == null) {
            return false;
        }
        try {
            Boolean result = (Boolean) isWebComponentMethod.invoke(null, iProject);
            return result;
        } catch (Exception e) {
            return false;
        }
    }
    
    public static IContainer getWebContentContainer(IProject iProject) {
        if (getWebContentContainerMethod == null) {
            return null;
        }
        try {
            IContainer result = (IContainer) getWebContentContainerMethod.invoke(null, iProject);
            return result;
        } catch (Exception e) {
            return null;
        }
    }
    
    public static boolean wstPresent() {
        return isWebComponentMethod != null && getWebContentContainerMethod != null;
    }
}
