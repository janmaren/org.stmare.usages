package org.stmare.usages.ref;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.search.core.text.TextSearchMatchAccess;

public class ContainsStringRequestor extends org.eclipse.search.core.text.TextSearchRequestor {
    private Set<IFile> resultFiles = new HashSet<IFile>();
    
    private String string;
    
    public ContainsStringRequestor() {
    }
    
    public ContainsStringRequestor(String str) {
        this.string = str;
    }
    
    @Override
    public boolean acceptPatternMatch(TextSearchMatchAccess matchAccess) throws CoreException {
        if (string == null) {
            resultFiles.add(matchAccess.getFile());
            return true;
        }
        String fileContent = matchAccess.getFileContent(0, matchAccess.getFileContentLength());
        if (fileContent.contains(string)) {
            resultFiles.add(matchAccess.getFile());
            return true;
        }
        return false;
    }

    @Override
    public boolean acceptFile(IFile file) throws CoreException {
        if (!file.isSynchronized(IResource.DEPTH_INFINITE)) {
            return false;
        }
        String extension = file.getFileExtension();
        if (extension != null) {
            extension = extension.toLowerCase();
            if ("class".equals(extension)) {
                return false;
            } else if ("jar".equals(extension)) {
                return false;
            } else if ("war".equals(extension)) {
                return false;
            } else if ("ear".equals(extension)) {
                return false;
            } else if ("zip".equals(extension)) {
                return false;
            }
        }
        
        return true;
    }

    public List<IFile> getResultFiles() {
        return new ArrayList<IFile>(resultFiles);
    }
}
