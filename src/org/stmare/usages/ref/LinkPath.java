/**
 * Copyright (c) 2010 Stanislav Marencik
 */
package org.stmare.usages.ref;

import org.eclipse.core.resources.IFile;

/**
 * Referencing link found in a file 
 */
public class LinkPath implements Comparable<LinkPath> {
	/**
	 * Absolute or relative path to other file
	 * Context can be in the beginning
	 */
	private String path;

	/**
	 * File where link is
	 */
	private IFile referencingFile;
	
	/**
	 * Position of link in file
	 */
	private int position = -1;
	
	public LinkPath(String path) {
		this.path = path;
	}

    public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public IFile getReferencingFile() {
		return referencingFile;
	}

	public void setReferencingFile(IFile file) {
		this.referencingFile = file;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return path;
	}


    @Override
    public int compareTo(LinkPath o) {
        if (o == null)
            return -1;
        if (!o.getReferencingFile().equals(getReferencingFile())) {
            return o.getReferencingFile().getName().compareTo(getReferencingFile().getName());
        }
        return getPosition() - o.getPosition();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((path == null) ? 0 : path.hashCode());
        result = prime * result + position;
        result = prime * result + ((referencingFile == null) ? 0 : referencingFile.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LinkPath other = (LinkPath) obj;
        if (path == null) {
            if (other.path != null)
                return false;
        } else if (!path.equals(other.path))
            return false;
        if (position != other.position)
            return false;
        if (referencingFile == null) {
            if (other.referencingFile != null)
                return false;
        } else if (!referencingFile.equals(other.referencingFile))
            return false;
        return true;
    }
	
}
