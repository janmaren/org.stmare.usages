package org.stmare.usages.ref;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.ProgressMonitorWrapper;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.search.core.text.TextSearchEngine;
import org.eclipse.search.ui.text.FileTextSearchScope;
import org.stmare.hyperlink.ResourceFinder;
import org.stmare.usages.Config;
import org.stmare.usages.view.UsageNodeEntity;
import org.stmare.util.HandlerUtil;
import org.stmare.util.StringUtil;
import org.stmare.util.VariousHelper;

public class ReferencesFinder {
    /**
     * Parent under which the children will be added by search
     */
    private UsageNodeEntity parent;
    
    private IProgressMonitor progressMonitor;
    
    public ReferencesFinder(UsageNodeEntity parent) {
        super();
        this.parent = parent;
    }
    
    public ReferencesFinder(UsageNodeEntity parent, IProgressMonitor progressMonitor) {
        super();
        this.parent = parent;
        this.progressMonitor = progressMonitor;
    }
    
    public void addReferencingFiles() throws OperationCanceledException {
        IFile referencedFile = parent.getFile();
        if (referencedFile != null) {
            if (referencedFile.getName().endsWith(".java")) {
                addReferencingFilesForJava(referencedFile);
            } else {
                addReferencingResourceFiles(referencedFile);
            }
        } else {
            String className = parent.getClassName();
            if (className == null) {
                throw new RuntimeException("Unknown referenced file in node");
            }
            addReferencingFilesForClass(className, referencedFile);
        }
    }

    private void addReferencingFilesForJava(IFile referencedFile) throws OperationCanceledException {
        String referencedCanonClassName = HandlerUtil.findClassName(referencedFile);
        if (referencedCanonClassName != null) {        
            addReferencingFilesForClass(referencedCanonClassName, referencedFile);
        }
    }

    public void addReferencingFilesForClass(String referencedCanonClassName, IResource resource) throws OperationCanceledException {
        String[] propPatterns = {"*"};
        IResource[] roots = new IResource[] {resource};
        //FileTextSearchScope scope = FileTextSearchScope.newWorkspaceScope(propPatterns, false);
        FileTextSearchScope scope = FileTextSearchScope.newSearchScope(roots, propPatterns, false);
        
        List<IFile> foundFilesCandidates = getFilesContainingString(referencedCanonClassName, progressMonitor, scope);
        boolean inLine = Config.getInLine();
        boolean even = true;
        for (IFile referencingFile : foundFilesCandidates) {
            if ("java".equals(referencingFile.getFileExtension())) {
                // references java to java is not aim (but could be), only resource to java, so rather skip
                continue;
            }
            if (isInDirectoryProject(referencingFile)) {
                // when it's not project but infact only directory containing project then ignore
                continue;
            }
            if (isInOutputDirectory(referencingFile)) {
                continue;
            }
            List<LinkPath> matchingLinkPaths = getMatchingLinkPathsForJava(referencingFile, referencedCanonClassName);
            if (matchingLinkPaths.size() > 0) {
                ReferencesFinder.createNodesInRequiredWay(parent, referencingFile, matchingLinkPaths, inLine, even = !even);
            }
        }
    }

    private void addReferencingResourceFiles(IFile referencedFile) throws OperationCanceledException {
        String name = referencedFile.getName();
   
        String[] propPatterns = {"*"};
        IResource[] roots = new IResource[] {referencedFile.getProject()};
        //FileTextSearchScope scope = FileTextSearchScope.newWorkspaceScope(propPatterns, false);
        FileTextSearchScope scope = FileTextSearchScope.newSearchScope(roots, propPatterns, false);
        List<IFile> foundFilesCandidates = getFilesContainingString(name, progressMonitor, scope);
        
        boolean inLine = Config.getInLine();
        boolean even = true;
        for (IFile referencingFile : foundFilesCandidates) {
            if (isInDirectoryProject(referencingFile)) {
                // when it's not project but infact only directory containing project then ignore
                continue;
            }
            if (isInOutputDirectory(referencingFile)) {
                continue;
            }            
            List<LinkPath> matchingLinkPaths = getMatchingLinkPaths(referencingFile, referencedFile);
            if (matchingLinkPaths.size() > 0) {
                ReferencesFinder.createNodesInRequiredWay(parent, referencingFile, matchingLinkPaths, inLine, even = !even);
            }
        }
    }

    private List<LinkPath> getMatchingLinkPathsForJava(IFile referencingFile, String referencedCanonClassName) {
        List<LinkPath> matchingLinkPaths = new ArrayList<LinkPath>();
        String string = VariousHelper.readFileAsString(referencingFile);
        int index = -1;
        do {
            index = string.indexOf(referencedCanonClassName, index + 1);
            if (index != -1) {
                InFilePos inFilePos = getInFilePosForJava(referencedCanonClassName, string, index);
                if (inFilePos != null) {
                    LinkPath linkPath = new LinkPath(inFilePos.resource);
                    linkPath.setReferencingFile(referencingFile);
                    linkPath.setPosition(inFilePos.start);
                    matchingLinkPaths.add(linkPath);
                }
            }
        } while (index != -1); 
        return matchingLinkPaths;
    }
    
    private List<LinkPath> getMatchingLinkPaths(IFile referencingFile, IFile referencedFile) {
        List<LinkPath> matchingLinkPaths = new ArrayList<LinkPath>();
        String string = VariousHelper.readFileAsString(referencingFile);
        int index = -1;
        String name = referencedFile.getName();
        do {
            index = string.indexOf(name, index + 1);
            if (index != -1) {
                InFilePos inFilePos = getInFilePos(name, string, index);
                // note: null is when name is 'file.xml' but there is 'file.xml.something' ('.something' behind)
                if (inFilePos != null) {
                    ResourceFinder resourceFinder = new ResourceFinder(referencingFile, inFilePos.resource);
                    List<IFile> foundReferences = resourceFinder.find();
                    if (foundReferences.contains(referencedFile)) {
                        LinkPath linkPath = new LinkPath(inFilePos.resource);
                        linkPath.setReferencingFile(referencingFile);
                        linkPath.setPosition(inFilePos.start);
                        matchingLinkPaths.add(linkPath);
                    }
                }
            }
        } while (index != -1); 
        return matchingLinkPaths;
    }

    private InFilePos getInFilePos(String name, String string, int nameStartPosition) {
        String linkString = StringUtil.getLinkStringByLastPart(string, nameStartPosition, nameStartPosition + name.length());
        if (linkString == null) {
            // doesn't end with terminator but the name continues
            return null;
        }
        InFilePos inFilePos = new InFilePos();
        inFilePos.resource = linkString;
        inFilePos.end = nameStartPosition + name.length();
        inFilePos.start = inFilePos.end - inFilePos.resource.length();
        
        return inFilePos;
    }

    private InFilePos getInFilePosForJava(String name, String string, int nameStartPosition) {
        if (!StringUtil.isTerminatorAround(string, nameStartPosition, nameStartPosition + name.length())) {
            return null;
        }
        InFilePos inFilePos = new InFilePos();
        inFilePos.resource = name;
        inFilePos.end = nameStartPosition + name.length();
        inFilePos.start = inFilePos.end - inFilePos.resource.length();
        
        return inFilePos;
    }
    
    public static void createNodesInRequiredWay(UsageNodeEntity parent, IFile referencingFile,
                    List<LinkPath> matchingLinkPaths, boolean inLine, boolean even) {
        if (!inLine) {
            for (LinkPath linkPath : matchingLinkPaths) {
                ArrayList<LinkPath> oneLinkPathForNode = new ArrayList<LinkPath>();
                oneLinkPathForNode.add(linkPath);
                UsageNodeEntity usageNodeEntity = new UsageNodeEntity(parent);
                usageNodeEntity.setFile(referencingFile);
                usageNodeEntity.setLinkPaths(oneLinkPathForNode);
                usageNodeEntity.setEven(even);
            }
        } else {
            UsageNodeEntity usageNodeEntity = new UsageNodeEntity(parent);
            usageNodeEntity.setFile(referencingFile);
            usageNodeEntity.setLinkPaths(matchingLinkPaths);
        }
    }

    private static List<IFile> getFilesContainingString(String name, IProgressMonitor progressMonitor, FileTextSearchScope scope) throws OperationCanceledException {
        IProgressMonitor progressMonitorForSearch = null;
        if (progressMonitor != null) {
            // wrap monitor to override searching label
            progressMonitorForSearch = new ProgressMonitorWrapper(progressMonitor) {
                public void beginTask(String name, int totalWork) {
                    this.getWrappedProgressMonitor().beginTask("Searching", totalWork);
                }
    
                @Override
                public boolean isCanceled() {
                    return this.getWrappedProgressMonitor().isCanceled();
                }
            };
        }
        
        ContainsStringRequestor containsStringRequestor = new ContainsStringRequestor(name);
        
        Pattern pattern = TextSearchEngine.createPattern(name, true, true);
        TextSearchEngine.createDefault().search(scope, containsStringRequestor, pattern, progressMonitorForSearch);
        List<IFile> resultIFiles = containsStringRequestor.getResultFiles();
        return resultIFiles;
    }

    public static boolean isInDirectoryProject(IFile file) {
        IProject iProject = file.getProject();
        IJavaProject javaProject = JavaCore.create(iProject);
        try {
            javaProject.getRawClasspath(); // when it's directory it throws exception. Is there better detection?
        } catch (JavaModelException e) {
            // exception used to detect
            return true;
        }
        return false;
        
    }

    public static boolean isInOutputDirectory(IFile file) {
        IProject iProject = file.getProject();
        IJavaProject javaProject = JavaCore.create(iProject);
        try {
            IPath outputLocation = javaProject.getOutputLocation();
            if (file.getFullPath().toString().startsWith(outputLocation.toString() + "/")) {
                return true;
            }
        } catch (JavaModelException e1) {
            // it hasn't output directory
            return false;
        }
        try {
            IClasspathEntry[] rawClasspath = javaProject.getRawClasspath();
            for (IClasspathEntry iClasspathEntry : rawClasspath) {
                IPath outputLocation = iClasspathEntry.getOutputLocation();
                if (outputLocation != null) {
                    if (file.getFullPath().toString().startsWith(outputLocation.toString() + "/")) {
                        return true;
                    }
                }
            }
        } catch (JavaModelException e) {
            return false;
        }
        return false;
    }

    static class InFilePos {
        public int start;
        
        public int end;
        
        public String resource;

        @Override
        public String toString() {
            return "InFilePos [value=" + resource + ", start=" + start + ", end=" + end + "]";
        }
    }
}
