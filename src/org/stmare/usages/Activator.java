/**
 * Copyright (c) 2010 Stanislav Marencik
 */
package org.stmare.usages;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.stmare.wst.J2EEUtilsAdapter;


/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.stmare.usages";

	// The shared instance
	private static Activator plugin;

	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		if (!J2EEUtilsAdapter.wstPresent()) {
		    logInfo("No WST present - web based calculation will not be used");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

    public static void logDebug(String message) {
        // SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:m:s.S");
    	// plugin.getLog().log(new Status(IStatus.INFO, PLUGIN_ID, simpleDateFormat.format(new Date()) + ":" + message));  //$NON-NLS-1$

//        if (plugin.isDebugging()) {
//        	plugin.getLog().log(new Status(IStatus.INFO, PLUGIN_ID, IStatus.OK,
//                    message, null));
//        }
    }

    public static void logError(Throwable t) {
    	plugin.getLog().log(new Status(IStatus.ERROR, PLUGIN_ID, IStatus.ERROR, "Internal Error in org.stmare plugin", t));  //$NON-NLS-1$
    }

    public static void logError(Throwable t, String message) {
        plugin.getLog().log(new Status(IStatus.ERROR, PLUGIN_ID, IStatus.ERROR, message, t));  //$NON-NLS-1$
    }

    public static void logInfo(String message) {
    	plugin.getLog().log(new Status(IStatus.INFO, PLUGIN_ID, message));  //$NON-NLS-1$
    }

    public static void logWarn(String message) {
    	//plugin.getLog().log(new Status(IStatus.WARNING, PLUGIN_ID, message));  //$NON-NLS-1$
    }

    public static void logWarn(Throwable t) {
    	//plugin.getLog().log(new Status(IStatus.WARNING, PLUGIN_ID, IStatus.WARNING, "Caught exception", t));  //$NON-NLS-1$
    }

    public static void logWarn(Throwable t, String message) {
    	//plugin.getLog().log(new Status(IStatus.WARNING, PLUGIN_ID, IStatus.WARNING, message, t));  //$NON-NLS-1$
    }


}
