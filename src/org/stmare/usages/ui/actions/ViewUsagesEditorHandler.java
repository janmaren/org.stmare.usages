/**
 * Copyright (c) 2010 Stanislav Marencik
 */
package org.stmare.usages.ui.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.stmare.usages.view.UsagesView;

/**
 * Action generated by shortcut
 */
public class ViewUsagesEditorHandler extends AbstractHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		IEditorPart activeEditor = HandlerUtil.getActiveEditor(event);
		IFile file = org.stmare.util.HandlerUtil.findSelectedIFileInEditor(activeEditor);
		if (file != null) {
            UsagesView.showTreeByFileSelection(file);
		}
		String className = org.stmare.util.HandlerUtil.findSelectedClassNameInEditor(activeEditor);
		if (className != null) {
		    UsagesView.showTreeByClassSelection(className);
		}
		return null;
	}
}
