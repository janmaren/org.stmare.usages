package org.stmare.usages.ui.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

public class EditorPropertyTester extends org.eclipse.core.expressions.PropertyTester {

    @Override
    public boolean test(Object object, String testName, Object[] arg2, Object value) {
        IWorkbenchPart activePart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePart();
        if (activePart instanceof IEditorPart) {
            IResource resource = (IResource)((IEditorPart) activePart).getEditorInput().getAdapter(IResource.class);
            if (resource instanceof IFile) {
                return true;
            }
        } 
        return false;
    }
}
