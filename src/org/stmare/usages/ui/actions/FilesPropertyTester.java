package org.stmare.usages.ui.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.jdt.core.IClassFile;


public class FilesPropertyTester extends org.eclipse.core.expressions.PropertyTester {

    @Override
    public boolean test(Object obj, String testName, Object[] arg2, Object value) {
        if (obj instanceof IClassFile) {
            return true;
        }
        if (obj instanceof org.eclipse.jdt.core.ICompilationUnit) {
            return true;
        }
        if (obj instanceof IFile) {
            return true;
        }
        return false;
    }
}
