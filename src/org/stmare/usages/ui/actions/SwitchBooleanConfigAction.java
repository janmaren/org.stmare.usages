package org.stmare.usages.ui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import org.stmare.usages.Config;
import org.stmare.usages.view.UsageNodeEntity;
import org.stmare.usages.view.UsagesView;
import org.stmare.usages.view.ViewContentProvider;

public class SwitchBooleanConfigAction extends Action {

    private final UsagesView usagesView;
    private final ViewContentProvider viewContentProvider;
    private final String configName;

    public SwitchBooleanConfigAction(String text, String configName, UsagesView usagesView, ViewContentProvider viewContentProvider, Image imageFlat) {
        super(text, IAction.AS_CHECK_BOX);
        this.configName = configName;
        this.usagesView = usagesView;
        this.viewContentProvider = viewContentProvider;
        setImageDescriptor(ImageDescriptor.createFromImage(imageFlat));
        boolean selected = Config.getBoolean(configName);
        setChecked(selected);
    }

    @Override
    public void run() {
        boolean selected = Config.getBoolean(configName);
        selected = !selected;
        Config.setBoolean(configName, selected);
        setChecked(selected);

        Object parent = viewContentProvider.getParent(null);
        if (parent instanceof UsageNodeEntity) {
            UsageNodeEntity usageParent = (UsageNodeEntity) parent;
            UsageNodeEntity root = new UsageNodeEntity();
            root.setFile(usageParent.getFile());
            usagesView.setRootUsageNodeEntity(root);
        }
    }
}
