/**
 * Copyright (c) 2010 Stanislav Marencik
 */
package org.stmare.usages.ui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.part.FileEditorInput;
import org.stmare.usages.view.UsagesView;

/**
 * Action launched in popup over editor
 */
public class ViewUsagesActionDelegate implements IEditorActionDelegate {
	private IEditorPart editor;

	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		this.editor = targetEditor;
	}

	public void run(IAction action) {
		IEditorInput editorInput = editor.getEditorInput();
		if (editorInput != null && editorInput instanceof FileEditorInput) {
			UsagesView.showTreeByFileSelection(((FileEditorInput)editorInput).getFile());
		}
	}

	public void selectionChanged(IAction arg0, ISelection arg1) {
	}

}
