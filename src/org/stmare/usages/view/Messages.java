package org.stmare.usages.view;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
    private static final String BUNDLE_NAME = "org.stmare.usages.view.messages"; //$NON-NLS-1$
    public static String ResourceHyperlinkDetector_open;
    public static String ResourceHyperlinkDetector_resource;
    public static String UsagesView_inline;
    public static String UsagesView_alsoOutput;
    public static String UsagesView_nosearch;
    public static String UsagesView_unabletoopen;
    public static String ViewContentProvider_Searching;
    public static String ViewContentProvider_UsageHierarchy;
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
