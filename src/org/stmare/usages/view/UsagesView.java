/**
 * Copyright (c) 2010 Stanislav Marencik
 */
package org.stmare.usages.view;

import java.util.HashMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.PageBook;
import org.eclipse.ui.part.ViewPart;
import org.stmare.usages.Activator;
import org.stmare.usages.Config;
import org.stmare.usages.ref.LinkPath;
import org.stmare.usages.ui.actions.SwitchBooleanConfigAction;

/**
 * View with hierarchy of usages
 */
public class UsagesView extends ViewPart {
	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "org.stmare.usages.UsagesView"; //$NON-NLS-1$

	private TreeViewer viewer;

	private ViewContentProvider viewContentProvider;

	private PageBook pageBook;

	private Label noUsagesSearchedLabel;

	private Composite treeViewerComposite;

	private Composite noUsagesSearchedLabelComposite;

    private Image imageFlat = new  Image(null, SwitchBooleanConfigAction.class.getResourceAsStream("/icons/flat.gif")); //$NON-NLS-1$

    private Image imageOutput = new  Image(null, SwitchBooleanConfigAction.class.getResourceAsStream("/icons/output.png")); //$NON-NLS-1$

    private static Image imageBinary = new  Image(null, SwitchBooleanConfigAction.class.getResourceAsStream("/icons/nodeBinary.png")); //$NON-NLS-1$

    private static Image imageOdd = new  Image(null, SwitchBooleanConfigAction.class.getResourceAsStream("/icons/nodeSmall.png")); //$NON-NLS-1$

	public static final UsagesView open() {
		IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		try {
			UsagesView usagesView = (UsagesView) activeWorkbenchWindow.getActivePage().showView(UsagesView.ID);
			return usagesView;
		} catch (PartInitException e) {
			throw new RuntimeException(Messages.UsagesView_unabletoopen, e);
		}
	}

	private static final void openInEditor(LinkPath linkPath) {
		IFile file = linkPath.getReferencingFile();
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		HashMap<String, Object> map = new HashMap<String, Object>();
        if (linkPath.getPosition() >= 0) {
            map = new HashMap<String, Object>();
            map.put(IMarker.CHAR_START, new Integer(linkPath.getPosition()));
            map.put(IMarker.CHAR_END, new Integer(linkPath.getPosition() + linkPath.getPath().length()));
        }
        try {
            IMarker marker = file.createMarker(IMarker.TEXT);
            marker.setAttributes(map);
            IDE.openEditor(page, marker);
        } catch (Exception e) {
            Activator.logError(e);
        }
	}

    private static final void openInEditor(IFile file) {
        IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
        try {
            IDE.openEditor(page, file);
        } catch (PartInitException e) {
            Activator.logError(e);
        }
    }

	@Override
	public void createPartControl(Composite parent) {
	    Composite composite = new Composite(parent, SWT.NONE);
	    composite.setLayoutData (new GridData(SWT.FILL, SWT.FILL, true, true));
	    composite.setLayout (new FillLayout ());
	    pageBook = new PageBook (composite, SWT.NONE);

	    treeViewerComposite = new Composite(pageBook, SWT.NONE);
	    treeViewerComposite.setLayout(new FillLayout());
		viewer = new TreeViewer(treeViewerComposite, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewContentProvider = new ViewContentProvider(viewer);
		viewer.setContentProvider(viewContentProvider);
		viewer.setInput(getViewSite());
		viewer.setLabelProvider(new ViewLabelProvider());
		IDoubleClickListener listener = new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection)selection).getFirstElement();
				if (obj instanceof UsageNodeEntity) {
					UsageNodeEntity usageNodeEntity = (UsageNodeEntity) obj;
					if (usageNodeEntity.getFile() != null && !usageNodeEntity.getFile().getName().endsWith(".jar") &&
					        !usageNodeEntity.getFile().getName().endsWith(".war") &&
                            !usageNodeEntity.getFile().getName().endsWith(".ear") &&
                            !usageNodeEntity.getFile().getName().endsWith(".zip")) {
    					if (usageNodeEntity.hasLinkPath()) {
    					    final LinkPath firstLinkPath = usageNodeEntity.getFirstLinkPath();
    					    openInEditor(firstLinkPath);
    					}
    					else {
    					    openInEditor(usageNodeEntity.getFile());
    					}
					}
					// unable to open class by class name
				}
			}
		};
		viewer.addDoubleClickListener(listener);

		noUsagesSearchedLabelComposite = new Composite(pageBook, SWT.NONE);
		noUsagesSearchedLabelComposite.setLayout(new FillLayout());
		noUsagesSearchedLabel = new Label(noUsagesSearchedLabelComposite, SWT.NONE);
		noUsagesSearchedLabel.setText(Messages.UsagesView_nosearch);

		pageBook.showPage(noUsagesSearchedLabelComposite);

		createActions();
		createToolBar();
	}

    private void createActions() {
    }

    private void createToolBar() {
        IToolBarManager toolbarMGR = getViewSite().getActionBars().getToolBarManager();
        toolbarMGR.add(new Separator());
        toolbarMGR.add(new SwitchBooleanConfigAction(Messages.UsagesView_inline, Config.IN_LINE, this, viewContentProvider, imageFlat)); //$NON-NLS-2$
        //toolbarMGR.add(new SwitchBooleanConfigAction(Messages.UsagesView_alsoOutput, Config.ALSO_OUTPUT, this, viewContentProvider, imageOutput)); //$NON-NLS-2$
    }

	public void setRootUsageNodeEntity(UsageNodeEntity usageNodeEntity) {
		viewContentProvider.setRoot(usageNodeEntity);
		viewer.refresh();
		viewer.expandToLevel(usageNodeEntity, 1);

		pageBook.showPage(treeViewerComposite);
	}

	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	public static void showTreeByActiveEditor(IFile referencedFile) {
		UsagesView usagesView = open();
		UsageNodeEntity usageNodeEntity = new UsageNodeEntity();
		usageNodeEntity.setFile(referencedFile);
		usagesView.setRootUsageNodeEntity(usageNodeEntity);
	}

	public static void showTreeByFileSelection(IFile referencedFile) {
		UsagesView usagesView = open();
		UsageNodeEntity usageNodeEntity = new UsageNodeEntity();
		usageNodeEntity.setFile(referencedFile);
		usagesView.setRootUsageNodeEntity(usageNodeEntity);
	}

    public static void showTreeByClassSelection(String className) {
        UsagesView usagesView = open();
        UsageNodeEntity usageNodeEntity = new UsageNodeEntity();
        usageNodeEntity.setClassName(className);
        usagesView.setRootUsageNodeEntity(usageNodeEntity);        
    }
    
	private static class ViewLabelProvider extends LabelProvider {
		public String getText(Object obj) {
			if (obj instanceof UsageNodeEntity) {
				UsageNodeEntity entity = (UsageNodeEntity) obj;
				return entity.getDisplayName();
			}
			// It should be only PendingUsageNodeEntity
			return obj.toString();
		}

		public Image getImage(Object obj) {
			if (obj instanceof UsageNodeEntity) {
			   UsageNodeEntity usageNodeEntity = (UsageNodeEntity) obj;
			   if (usageNodeEntity.getClassName() != null)
			       return imageBinary;
			   return imageOdd;
			}
			return super.getImage(obj);
		}
	}


    @Override
    public void dispose() {
        imageFlat.dispose();
        imageOutput.dispose();
        imageOdd.dispose();
        imageBinary.dispose();
        super.dispose();
    }

}
