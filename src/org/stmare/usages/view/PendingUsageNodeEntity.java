/**
 * Copyright (c) 2010 Stanislav Marencik
 */
package org.stmare.usages.view;

/**
 * Node in tree when pending
 */
public class PendingUsageNodeEntity {
	@Override
	public String toString() {
		return "...";
	}
}
