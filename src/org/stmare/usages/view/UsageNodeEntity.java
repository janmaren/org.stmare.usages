/**
 * Copyright (c) 2010 Stanislav Marencik
 */
package org.stmare.usages.view;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.stmare.usages.ref.LinkPath;

/**
 * Node object in tree
 * In three possibly states:
 * 1) children == null && pendingChildren == false - children not fetched yet
 * 2) children == null && pendingChildren == true - fetching children in progress, prevent not to do it paralell again
 * 3) children != null && pendingChildren == false - children fetched (no need to do it again any more)
 * @author standa
 */
public class UsageNodeEntity {
    /**
     * Referenced {@ IFile}. Prefered field to be populated instead of {@link #className}
     */
	private IFile file;
	
	/**
	 * Referenced canonical class name. When {@link #file} is missing then className is populated
	 */
	private String className;

	private final UsageNodeEntity parent;

	private List<UsageNodeEntity> children = null;

	private List<LinkPath> linkPaths = new ArrayList<LinkPath>();

	private boolean pendingChildren;

	private Boolean even = null;

	public LinkPath getFirstLinkPath() {
		return linkPaths.get(0);
	}

	public boolean hasLinkPath() {
	    return linkPaths.size() > 0;
	}

	public void addLinkPath(LinkPath linkPath) {
		linkPaths.add(linkPath);
	}

	public UsageNodeEntity() {
		this.parent = null;
	}

	public UsageNodeEntity(UsageNodeEntity parent) {
		this.parent = parent;
		this.parent.addChild(this);
	}

	public String getFileName() {
		return file.getName();
	}

	public void addChild(UsageNodeEntity child) {
		if (children == null) {
			children = new ArrayList<UsageNodeEntity>();
		}
		children.add(child);
	}

	public UsageNodeEntity[] getChildren() {
		if (children == null)
			return null;
		return children.toArray(new UsageNodeEntity[children.size()]);
	}

	public UsageNodeEntity getParent() {
		return parent;
	}


	public boolean isPendingChildren() {
		return pendingChildren;
	}

	public void setPendingStarted() {
		this.pendingChildren = true;
	}

	public void setPendingFinished() {
		if (children == null) {
			children = new ArrayList<UsageNodeEntity>();
		}
		this.pendingChildren = false;
	}
    
	public void setUnknownChildren() {
        children = null;
    }
    
	public String getDisplayName() {
		StringBuilder sb = new StringBuilder();
		if (file != null) {
		    sb.append(file.getName());
	        sb.append(" - ").append(file.getFullPath());
		} else if (className != null) {
		    sb.append(className);
		} else {
		    throw new RuntimeException("Unknown referenced file");
		}
		if (linkPaths != null && linkPaths.size() > 0) {
		    for (LinkPath linkPath : linkPaths) {
                sb.append(" [");
                sb.append(linkPath.getPath());
                sb.append("]");
            }
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		return (file != null ? file.getFullPath() + "/" + file.getProjectRelativePath() : className) ;
	}

	public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public IFile getFile() {
		return file;
	}

	public void setFile(IFile referencedFile) {
		this.file = referencedFile;
	}

	public void setLinkPaths(List<LinkPath> matchingLinkPaths) {
		this.linkPaths = matchingLinkPaths;
	}

    public Boolean getEven() {
        return even;
    }

    public void setEven(Boolean even) {
        this.even = even;
    }
}
