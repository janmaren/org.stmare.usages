/**
 * Copyright (c) 2010 Stanislav Marencik
 */
package org.stmare.usages.view;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IViewSite;
import org.stmare.usages.Activator;
import org.stmare.usages.ref.ReferencesFinder;

/**
 * Data for view
 */
public class ViewContentProvider implements IStructuredContentProvider, ITreeContentProvider {
	private UsageNodeEntity root;

	private final TreeViewer viewer;

	private PendingUsageNodeEntity pendingUsageNodeEntity = new PendingUsageNodeEntity();

    private static ViewJob job;

	public ViewContentProvider(TreeViewer viewer) {
		this.viewer = viewer;
	}

	public void inputChanged(Viewer v, Object oldInput, Object newInput) {
	}

	public void dispose() {
	}

	public Object[] getElements(Object parent) {
		if (parent instanceof IViewSite && root != null) {
			Object[] rootArr = {root};
			return rootArr;
		}
		Object[] noArr = {};
		return noArr;
	}

	private void findChildren(final UsageNodeEntity parent, final Display display) {
	    TreeItem treeItem = findTreeItem(parent);
	    if (job != null) {
	        job.cancel();
	    }
	    job = new ViewJob(Messages.ViewContentProvider_UsageHierarchy, viewer, parent, display, treeItem);
	    job.setPriority(Job.LONG);
	    job.schedule();
	}

	private TreeItem findTreeItem(UsageNodeEntity usageNodeEntity) {
	    Tree tree = viewer.getTree();
	    TreeItem[] items = tree.getItems();
	    for (TreeItem item : items) {
	        TreeItem findTreeItem = findTreeItem(item, usageNodeEntity);
            if (findTreeItem != null) {
                return findTreeItem;
            }
        }
        return null;
	}

	private TreeItem findTreeItem(TreeItem parentItem, UsageNodeEntity usageNodeEntity) {
	    if (parentItem.getData() == usageNodeEntity) {
	        return parentItem;
	    }
        for (TreeItem item : parentItem.getItems()) {
            TreeItem findTreeItem = findTreeItem(item, usageNodeEntity);
            if (findTreeItem != null) {
                return findTreeItem;
            }
        }	    
	    return null;
	}
	       
	public synchronized Object[] getChildren(Object parent) {
		if (parent instanceof UsageNodeEntity) {
			UsageNodeEntity parentUsageNodeEntity = (UsageNodeEntity) parent;
			UsageNodeEntity[] children = parentUsageNodeEntity.getChildren();
			if (children == null) {
				if (!parentUsageNodeEntity.isPendingChildren()) {
					parentUsageNodeEntity.setPendingStarted();
					findChildren(parentUsageNodeEntity, viewer.getControl().getDisplay());
				}
				return new PendingUsageNodeEntity[] {pendingUsageNodeEntity};
			} else {
				return children;
			}
		}
		return null;
	}

	public Object getParent(Object child) {
		if (child instanceof UsageNodeEntity) {
			UsageNodeEntity usageNodeEntity = (UsageNodeEntity) child;
			return usageNodeEntity.getParent();
		}
		return root;
	}

	public boolean hasChildren(Object parent) {
		if (parent instanceof PendingUsageNodeEntity)
			return false;
		return true;
	}

	public void setRoot(UsageNodeEntity root) {
		this.root = root;
	}
	
	private static class ViewJob extends Job {
	    private TreeViewer viewer;
	    
	    private UsageNodeEntity parent; 
	    
	    private Display display;

        private TreeItem treeItem;
	    
        public ViewJob(String name, TreeViewer viewer, UsageNodeEntity parent, Display display, final TreeItem treeItem) {
            super(name);
            this.viewer = viewer;
            this.parent = parent;
            this.display = display;
            this.treeItem = treeItem;
        }

        @Override
        protected IStatus run(IProgressMonitor monitor) {
            try {
                monitor.beginTask(Messages.ViewContentProvider_Searching, 100);
                try {
                    new ReferencesFinder(parent, monitor).addReferencingFiles();
                } catch (OperationCanceledException e) {
                } catch (Exception e) {
                    Activator.logError(e);
                }
                parent.setPendingFinished();
                if (monitor.isCanceled()) {
                    display.asyncExec(new Runnable() {
                        public void run() {
                            treeItem.setExpanded(false);
                            parent.setUnknownChildren();
                        }
                    });
                }
                display.asyncExec(new Runnable() {
                    public void run() {
                        viewer.refresh(parent);
                    }
                });
            } finally {
                monitor.done();
            }
            return Status.OK_STATUS;
        }
	    
	}
}
