package org.stmare.usages;

import java.util.ArrayList;
import java.util.List;

public class Config {
    public static final String IN_LINE = "inLine";
    public static final String ALSO_OUTPUT = "alsoOutput";

    public static String[] getSearchedSuffixes() {
        String string = Activator.getDefault().getPreferenceStore().getString("searchedSuffixes");
        String[] suffixes = string.split(";");
        List<String> searchedSuffixes = new ArrayList<String>();
        for (String suffix : suffixes) {
            searchedSuffixes.add(suffix);
        }

        return searchedSuffixes.toArray(new String[searchedSuffixes.size()]);
    }

    public static boolean getCaseSensitive() {
        return Activator.getDefault().getPreferenceStore().getBoolean("caseSensitive");
    }
    
    public static boolean getInLine() {
        return Activator.getDefault().getPreferenceStore().getBoolean(IN_LINE);
    }
    
    public static void setInLine(boolean inLine) {
        Activator.getDefault().getPreferenceStore().setValue(IN_LINE, inLine);
    }

    public static boolean getBoolean(String configName) {
        return Activator.getDefault().getPreferenceStore().getBoolean(configName);
    }

    public static void setBoolean(String configName, boolean value) {
        Activator.getDefault().getPreferenceStore().setValue(configName, value);
    }
}
